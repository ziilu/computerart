import processing.serial.*;
import cc.arduino.*;
import ddf.minim.*;

Arduino arduino;
Minim minim;

int num = 8;
float margin = 50;
float size = 50;
Spot[][] field = new Spot[8][8];
AudioSample[] sound = new AudioSample[8];

Mytriangle tri;

void setup() {
  size(500, 550);
  frameRate(5);
//  arduino = new Arduino(this, Arduino.list()[0],57600);
//  arduino.digitalWrite(2,Arduino.HIGH);
//  arduino.pinMode(2,Arduino.INPUT);
  
  
  for(int i=0;i<num;i++){
    for(int j=0;j<num;j++){
    field[i][j] = new Spot(margin + size*i,margin+size*j);
    }
  }
  tri = new Mytriangle(75,500);
  
  minim = new Minim(this);
  sound[0] = minim.loadSample("s0.mp3",2048);
  sound[1] = minim.loadSample("s1.mp3",2048);
  sound[2] = minim.loadSample("s2.mp3",2048);
  sound[3] = minim.loadSample("s3.mp3",2048);
  sound[4] = minim.loadSample("s4.mp3",2048);
  sound[5] = minim.loadSample("s5.mp3",2048);
  sound[6] = minim.loadSample("s6.mp3",2048);
  sound[7] = minim.loadSample("s7.mp3",2048);


//  arduino = new Arduino(this, Arduino.list()[4], 57600);
//  arduino.pinMode(ledPin, Arduino.OUTPUT);
}
 
void draw(){
  background(0);
  if (keyPressed == true)
//  if(arduino.digitalRead(2) == Arduino.LOW)
      incCheck();
  drawfield();
  play();
  
  
  tri.display();
  tri.incCount();
}
void drawfield(){
  for(int i=0;i<num;i++){
    for(int j=0;j<num;j++){
    field[i][j].display();
    }
  }
}

void incCheck(){
 int c = 0;
 while(c<num && field[tri.count][c].changeCheck()){
   c++;
 }
}

void play(){
  for(int i=0;i<num;i++)
  if(field[tri.count][i].IsChecked)
    sound[i].trigger();
}


void stop() {

  super.stop();
}
