class Mytriangle {
  //member
  int count;
  int number;
  float margin;
  float x,y,size;

  //constructer
  Mytriangle(float x,float y){
    this(x,y,50,50);
  }
  Mytriangle(float x,float y,float size,float margin){
    this.x = x;
    this.y = y;
    this.size = size;
    this.margin = margin;
    count = 0;
    number = 8;
  }
  
  void incCount(){
    count++;
    if(count >= number)
      count = 0;
  }
  
  void display(){
      stroke(255,255,255);
      fill(255,255,255);
      triangle(x + margin*count, y-size/2, x-size/2+ margin*count, y+size/3, x+size/2+ margin*count, y+size/3);
  }
  void play(){
  
  }
}
