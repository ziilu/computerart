class Spot {
  //member
  boolean IsChecked;
  AudioSample audio;
  float x,y,size;
  
  Spot(){
    IsChecked = false;
    size = 50;
  }
  Spot(float x,float y){
    this.x = x;
    this.y = y;
    IsChecked = false;
    size = 50;
  }

  void display() {
    if(IsChecked){
      stroke(255, 255, 255);
      fill(255,255,255);
    }else{
      stroke(255,255,255);
      fill(0,0,0);
    }
    rect(x,y,size,size);
  }
  
  Boolean changeCheck(){
    if(IsChecked){
      IsChecked = false;
      return true;
    }
    else{
      IsChecked = true;
      return false;
    }
  }
}
